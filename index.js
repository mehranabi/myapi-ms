import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import axios from 'axios'
import moment from 'moment'
import { MD5 } from 'crypto-js'

const config = {
    api_url: 'https://api.myip.ms',
    api_id: 'xxxxxxx',
    api_key: 'xxxxxxxxxx-xxxxxxxxx-xxxxxxxxxx'
}

const api = axios.create({
    baseURL: config.api_url,
    timeout: 10000
})

const url_maker = (query) => {
    const { api_id, api_key, api_url } = config

    const api_id_part = `api_id/${api_id}`
    const api_key_part = `api_key/${api_key}`
    const common_part = `${query}/${api_id_part}/${api_key_part}`

    const timestamp = moment.utc().format('YYYY-MM-DD_HH:mm:ss')
    const timestamp_part = `timestamp/${timestamp}`

    const signature = MD5(`${api_url}/${common_part}/${timestamp_part}`)
    const signature_part = `signature/${signature}`

    return `${common_part}/${signature_part}/${timestamp_part}`
}

const App = () => {
    const [query, setQuery] = useState('')
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState('Empty')

    const onQueryChange = e => setQuery(e.target.value)
    const onSubmit = (e) => {
        if (query.length === 0) {
            alert("Enter Query!")
            return
        }

        setLoading(true)

        api.get(url_maker(query)).then(res => {
            setData(res.data)
            setLoading(false)
        }).catch(err => {
            setData(err.message)
            setLoading(false)
        })
    }

    return (
        <div>
            <label>Query:</label>
            <input value={query} onChange={onQueryChange} />
            <button onClick={onSubmit}>Submit</button>
            <hr />
            <div>
                {
                    loading ? 'Loading...' : (
                        <pre>
                            {JSON.stringify(data, undefined, 4)}
                        </pre>
                    )
                }
            </div>
        </div>
    )
}

ReactDOM.render(<App />, document.getElementById('app'))
